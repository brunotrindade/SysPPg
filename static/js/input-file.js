/**
 * Script para alterar o input[type=file]
 * Desenvolvido por: Bruno Trindade (2020)
*/

const inputButtonClick = (e) => {
    e.previousElementSibling.previousElementSibling.click()
}

const inputChange = (e) => {
    if(e.nextElementSibling.tagName == "INPUT")
        e.nextElementSibling.value = e.files[0].name
    else if(e.nextElementSibling.tagName == "BUTTON")
        e.nextElementSibling.style.filter = "brightness(100%)"
}