const config = {
    "language": {
        "lengthMenu": "_MENU_ por página",
        "zeroRecords": "Nenhum registro foi encontrado",
        "info": "Mostrando página _PAGE_ de _PAGES_",
        "infoEmpty": "Nenhum registro disponível",
        "infoFiltered": "(filtrado de um total de _MAX_ total registros)",
        "search": "Busca:",
        "paginate": {
            "first":      "Primeira",
            "last":       "Última",
            "next":       "Próxima",
            "previous":   "Anterior"
        },
    }
}