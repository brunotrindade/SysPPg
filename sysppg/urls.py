"""sysppg URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static, settings
from django.contrib.auth.decorators import login_required
from users.views import LoginView, LogoutView, SignUpView
from .settings import MEDIA_ROOT, MEDIA_URL
from django.views.generic import TemplateView
from notices import urls as notice_urls
from subscriptions import urls as subscription_urls
from subscriptions.views import ResourceCreate, ResourceUpdate, ResourceAnswerCreate
from base.views import DashboardView
from users import urls as user_urls
from . import views

urlpatterns = [
    path('', DashboardView.as_view(), name="dashboard"),
    path('admin/', admin.site.urls),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('login/', LoginView.as_view(), name='login'),
    path('signup/', SignUpView.as_view(), name='signup'),
    path('notice/', include(notice_urls)),
    path('subscription/', include(subscription_urls)),
    path('user/', include(user_urls)),
    path('stepresult/<uuid:pk>/resource', ResourceCreate.as_view(), name='resource_create'),
    path('resource/<uuid:pk>/update', ResourceUpdate.as_view(), name='resource_update'),
    path('resource/<uuid:pk>/answer', ResourceAnswerCreate.as_view(), name='resource_answer_create')
] + static(MEDIA_URL, document_root=MEDIA_ROOT)

handler403 = views.handler403
handler404 = views.handler404