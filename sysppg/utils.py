import os
from django.core.exceptions import ValidationError
from PyPDF2 import PdfFileReader, PdfFileWriter

# limit = size in megabytes
def validate_file_size(file, limit=10):
    if file.size > (limit * (2 ** 20)):
        raise ValidationError(f"O tamanho máximo que pode ser enviado é {limit} MB!") 

    return file

def validate_file_extension(file):
    ext = os.path.splitext(file.name)[1]
    if not ext.lower() == '.pdf':
        raise ValidationError("Pode ser enviado apenas arquivo em formato PDF.")

def merge_pdfs(files, buffer):
    input_streams = []
    try:
        for input_file in files:
            input_streams.append(open(input_file, 'rb'))
        writer = PdfFileWriter()
        for reader in map(PdfFileReader, input_streams):
            for n in range(reader.getNumPages()):
                writer.addPage(reader.getPage(n))

        writer.write(buffer)
    finally:
        for f in input_streams:
            f.close()