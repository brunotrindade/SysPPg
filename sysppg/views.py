from django.shortcuts import render
from django.template import RequestContext
 
def handler403(request, exception):
    return render(request, 'error.html', { "exception": exception, "code": 403 })
 
def handler404(request, exception):
    return render(request, 'error.html', { "exception": exception, "code": 404 })
 
def handler500(request, exception):
    return render(request, 'error.html', { "exception": exception, "code": 500 })