from django import template
from subscriptions.models import Subscription

register = template.Library()

@register.simple_tag
def is_user_subscribed(user, notice):
    return Subscription.objects.filter(user=user, notice=notice).exists()

@register.simple_tag
def is_program_coordinator(user, program):
    return user.programs.filter(pk=program.pk).exists()