from django import forms
from .models import *
from django.contrib.auth.forms import UsernameField
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import authenticate
from django.contrib import messages
from django.core.exceptions import ValidationError
from sysppg import settings

class NoticeForm(forms.ModelForm):
    opening_date = forms.DateField(input_formats=settings.DATE_INPUT_FORMATS, widget=forms.DateInput(
        attrs={'class': 'form-control form-control-lg', 'data-mask': '00/00/0000'}
    ))

    closing_date = forms.DateField(input_formats=settings.DATE_INPUT_FORMATS, widget=forms.DateInput(
        attrs={'class': 'form-control form-control-lg', 'data-mask': '00/00/0000'}
    ))

    start_date = forms.DateField(input_formats=settings.DATE_INPUT_FORMATS, widget=forms.DateInput(
        attrs={'class': 'form-control form-control-lg', 'data-mask': '00/00/0000'}
    ))

    end_date = forms.DateField(input_formats=settings.DATE_INPUT_FORMATS, widget=forms.DateInput(
        attrs={'class': 'form-control form-control-lg', 'data-mask': '00/00/0000'}
    ))

    class Meta:
        model = Notice
        fields = ('title', 'identifier', 'program', 'description', 'opening_date', 'closing_date', 'start_date', 'end_date')
        widgets = {
            'title': forms.TextInput(
                attrs={'class': 'form-control form-control-lg'}
            ),
            'identifier': forms.TextInput(
                attrs={'class': 'form-control form-control-lg'}
            ),
            'program': forms.Select(
                attrs={'class': 'form-control form-control-lg'}
            ),
            'description': forms.Textarea(
                attrs={'class': 'form-control form-control-lg', 'rows': '5'}
            ),
        }

    def __init__(self, *args, **kwargs):
        programs = kwargs.pop('programs', None)
        super(NoticeForm, self).__init__(*args, **kwargs)

        if programs:
            self.fields['program'].queryset = programs

    def clean(self):
        self.instance.status = Notice.Status.PENDING

        opening = self.cleaned_data['opening_date']
        close = self.cleaned_data['closing_date']

        start = self.cleaned_data['start_date']
        end = self.cleaned_data['end_date']

        if(opening >= close):
            raise forms.ValidationError(_("A data de abertura do edital deve ser antes da data de fechamento"))
        if(opening > start):
            raise forms.ValidationError(_("A data de início das inscrições deve ser após a data de abertura de edital"))
        if(close <= start):
            raise forms.ValidationError(_("A data de início das inscrições deve ser antes da data de fechamento do edital"))
        if(start >= end ):
            raise forms.ValidationError(_("A data de fechamento das incrições deve ser após a data de incío das inscrições"))               
        if(close <= end):
            raise forms.ValidationError(_("A data de fechamento das incrições deve ser antes da data fechamento do edital"))
        
class VacancyForm(forms.ModelForm):
    class Meta:
        model = Vacancy
        fields = ('quantity', 'modality')
        widgets = {
            'quantity': forms.TextInput(
                attrs={'class': 'form-control form-control-lg'}
            ),
            'modality': forms.Select(
                attrs={'class': 'form-control form-control-lg'}
            ),
        }


class NoticeVacancyForm(forms.BaseInlineFormSet):
    def clean(self):
        if any(self.errors):
            return
        # Validar os campos

class NoticeStepForm(forms.ModelForm):
    release_date = forms.DateField(input_formats=settings.DATE_INPUT_FORMATS, widget=forms.DateInput(
        attrs={'class': 'form-control form-control-lg', 'data-mask': '00/00/0000'}
    ))

    resource_limit_date = forms.DateField(input_formats=settings.DATE_INPUT_FORMATS, widget=forms.DateInput(
        attrs={'class': 'form-control form-control-lg', 'data-mask': '00/00/0000'}
    ))

    class Meta:
        model = NoticeStep
        fields = ('order', 'step', 'passing_score', 'release_date', 'resource_limit_date')
        widgets = {
            'order': forms.HiddenInput(),
            'step': forms.Select(
                attrs={'class': 'form-control form-control-lg'}
            ),
            'passing_score': forms.TextInput(
                attrs={'class': 'form-control form-control-lg'}
            )
        }

    def clean_passing_score(self):
        passing_score = self.cleaned_data.get('passing_score')
        if(passing_score < 0):
            raise forms.ValidationError(_("A nota de corte não pode ser negativa."))

        return passing_score

class NoticeNoticeStepForm(forms.BaseInlineFormSet):
    def clean(self):
        order = 1
        used_steps = {}
        step = self.forms[0].instance
        for form in  self.forms:
            form.instance.order = order
            try:                
                if any(self.errors):
                    return

                if form.instance.step.is_subscription_step():
                    step = form.instance
                else:
                    if form.instance.release_date <= step.resource_limit_date:
                        raise ValidationError(_(f"A data de realização da {order}ª etapa deve ser após a data limite de recurso da {step.order}ª etapa"))

                    step = form.instance
                    

                if step.release_date >= step.resource_limit_date:
                    raise ValidationError(_(f"A data de realização da {step.order}ª etapa deve ser antes da data de limite de recurso"))

                if form.instance.step.pk in used_steps:
                    raise ValidationError(f"A {order}ª etapa já está presente neste edital.")
                else:
                    used_steps[form.instance.step.pk] = True

                if order == 1 and not form.instance.step.is_subscription_step():
                    raise ValidationError(f"A avaliação de inscrição deve ser a primeira etapa.")
                order += 1
            except Step.DoesNotExist:
                continue

           


class NoticeAttachmentForm(forms.ModelForm):
    class Meta:
        model = NoticeAttachment
        fields = ('name', 'type', 'file')
        widgets = {
            'name': forms.TextInput(
                attrs={'class': 'form-control form-control-lg'}
            ),
            'type': forms.Select(
                attrs={'class': 'form-control form-control-lg'}
            ),
            'file': forms.FileInput(
                attrs={'class': 'form-control form-control-lg'}
            )
        }

class NoticeNoticeAttachmentForm(forms.BaseInlineFormSet):
    def clean(self):
        if any(self.errors):
            return

        # Validar os campos

class RequiredDocumentForm(forms.ModelForm):
    class Meta:
        model = RequiredDocument
        fields = ('name', 'file', 'type', 'optional')
        widgets = {
             'name': forms.TextInput(
                attrs={'class': 'form-control form-control-lg'}
            ),
            'type': forms.Select(
                attrs={'class': 'form-control form-control-lg'}
            ),
            'file': forms.FileInput(
                attrs={'class': 'form-control form-control-lg'}
            ),
            'optional': forms.Select(
                attrs={'class': 'form-control form-control-lg'},
                choices=[(False, "Não"), (True, ("Sim"))]
            ),
        }

    def clean_file(self):
        return self.cleaned_data['file']

class NoticeRequiredDocumentForm(forms.BaseInlineFormSet):
    def clean(self):
        if any(self.errors):
            return

        # Validar os campos

NoticeFormSet = forms.inlineformset_factory(
    Notice, Vacancy, VacancyForm, NoticeVacancyForm, extra=1, can_delete=True, min_num=1, validate_min=True
)

NoticeNoticeStepFormSet = forms.inlineformset_factory(
    Notice, NoticeStep, NoticeStepForm, NoticeNoticeStepForm, extra=0, can_delete=True, min_num=1, validate_min=True
)

NoticeNoticeAttachmentFormSet = forms.inlineformset_factory(
    Notice, NoticeAttachment, NoticeAttachmentForm, NoticeNoticeAttachmentForm, extra=1, can_delete=True
)
NoticeRequiredDocumentFormSet = forms.inlineformset_factory(
    Notice, RequiredDocument, RequiredDocumentForm, NoticeRequiredDocumentForm, extra=1, can_delete=True, min_num=1, validate_min=True
)
