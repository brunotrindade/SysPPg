from django.urls import path
from django.contrib.auth.decorators import login_required
from .views import *
from django.views.generic import TemplateView
from subscriptions.views import SubscriptionCreate


urlpatterns = [
    path('', NoticeList.as_view(), name='notice_list'),
    path('add/', NoticeCreate.as_view(), name='notice_add'),
    path('all/', NoticeListAll.as_view(), name='notice_list_all'),
    path('finished/', NoticeListFinished.as_view(), name='notice_list_finished'),
    path('open/', NoticeListOpen.as_view(), name='notice_list_open'),
    path('search/', NoticeSearch.as_view(), name='notice_search'),
    path('<uuid:pk>/query', NoticeQuery.as_view(), name="notice_query_view"),
    path('<uuid:pk>/update', NoticeUpdate.as_view(), name='notice_update'),
    path('<uuid:pk>/cancel', NoticeCancel.as_view(), name='notice_cancel'),
    path('<uuid:pk>/subscribe', SubscriptionCreate.as_view(), name='subscription_add'),
    path('<uuid:pk>/view', NoticeView.as_view(), name='notice_view'),
    path('<uuid:pk>/steps_result', StepsResult.as_view(), name='steps_result_view'),
    path('<uuid:pk>/result', NoticeResult.as_view(), name='notice_result_view'),
    path('<uuid:pk>/subscriptions', NoticeSubscriptions.as_view(), name='notice_subscriptions'),
    path('report/results', NoticeResultReport.as_view(), name='notice_result_report'),
    path('report/subscriptions', NoticeSubscriptionsReport.as_view(), name='notice_subscriptions_report')
]