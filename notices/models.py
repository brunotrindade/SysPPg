from django.db import models
from django.utils.translation import gettext as _
from base.models import BaseModel, Modality, Step
from users.models import User
from base.models import PostGraduationProgram
from django.utils import timezone
from sysppg.utils import validate_file_size, validate_file_extension

class Notice(BaseModel):
    class Status(models.TextChoices):
        PENDING = 'P', _('Pendente')
        IN_PROGRESS = 'A', _('Em andamento')
        CANCELED = 'X', _('Cancelado')
        FINISHED = 'C', _('Concluído')

    title = models.CharField(_('Título'), max_length=40)
    identifier = models.CharField(_('Identificador'), max_length=15)
    opening_date = models.DateField(_('Data de abertura'))
    closing_date = models.DateField(_('Data de encerramento'))
    start_date = models.DateField(_('Data de início das inscrições'))
    end_date = models.DateField(_('Data de fim das inscrições'))
    status = models.CharField(_('Status'), max_length=1, choices=Status.choices, default=Status.PENDING)
    description = models.TextField(_('Descrição'))
    program = models.ForeignKey(PostGraduationProgram, on_delete=models.PROTECT)
    number_views = models.IntegerField(_("Visualições"), default=0)

    class Meta:
        verbose_name = _('Edital')
        verbose_name_plural = _('Editais')

    def __str__(self):
        return f'{self.title}'

    def get_status_color(self):
        if self.status in ['P', 'A']:
            return 'bg-warning'
        elif self.status in ['X', 'D']:
            return 'bg-danger'
        else:
            return 'bg-success'

    def can_be_canceled(self):
        return self.status in [self.Status.PENDING, self.Status.IN_PROGRESS]

    def can_subscribe(self):
        now = timezone.now().date()
        return now >= self.start_date and now <= self.end_date and self.status == self.Status.PENDING

    def has_addendum(self):
        return NoticeAttachment.Type.ADDENDUM in \
            [atm.type for atm in self.attachments.all()]


class Vacancy(BaseModel):
    quantity = models.IntegerField(_('Quantidade'))
    modality = models.ForeignKey(Modality, on_delete=models.PROTECT)
    notice = models.ForeignKey(Notice, on_delete=models.CASCADE, related_name="vacancies")

    class Meta:
        verbose_name = _('Vaga para Modalidade de Edital')
        verbose_name_plural = _('Vagas para Modalidade de Edital')

    def __str__(self):
        return f'{self.notice.title} - {self.modality.name}'

class NoticeAttachment(BaseModel):
    class Type(models.TextChoices):
        ADDENDUM = 'A', _('Adendo')
        MODEL = 'M', _('Modelo')
        EXAM_SHEETS = 'C', _('Caderno de Provas')

    name = models.CharField(_('Nome'), max_length=25)
    type = models.CharField(_('Tipo'), max_length=1, choices=Type.choices, default=Type.ADDENDUM)
    file = models.FileField(_('Arquivo'), upload_to='uploads/', validators=[validate_file_size, validate_file_extension])
    notice = models.ForeignKey(Notice, on_delete=models.CASCADE, related_name="attachments")

    class Meta:
        verbose_name = _('Anexo de Edital')
        verbose_name_plural = _('Anexos de Edital')

    def __str__(self):
        return f'{self.notice.title} - {self.name}'

class NoticeStep(BaseModel):
    order = models.IntegerField(_('Ordem'), default=0)
    passing_score = models.DecimalField(_('Nota de corte'), max_digits=4, decimal_places=2)
    release_date = models.DateField(_('Data de publicação'))
    resource_limit_date = models.DateField(_('Data de limite para recurso'))
    notice = models.ForeignKey(Notice, on_delete=models.CASCADE, related_name="notice_steps")
    step = models.ForeignKey(Step, on_delete=models.PROTECT)

    class Meta:
        verbose_name = _('Etapa de Edital')
        verbose_name_plural = _('Etapa de Edital')

    def __str__(self):
        return f'{self.notice.title} - {self.step.name}'

    def can_send_resource(self):
        return timezone.now().date() <= self.resource_limit_date 

class RequiredDocument(BaseModel):
    class Type(models.TextChoices):
        FILE = 'A', _('Envio de arquivo')
        TEXT = 'T', _('Apenas texto')

    name = models.CharField(_('Nome'), max_length=50)
    file = models.FileField(_('Arquivo'), upload_to='uploads/', blank=True, validators=[validate_file_size, validate_file_extension])
    type = models.CharField(_('Tipo'), max_length=1, choices=Type.choices, default=Type.FILE)
    optional = models.BooleanField(_('Opcional'))
    notice = models.ForeignKey(Notice, on_delete=models.CASCADE, related_name="required_documents")

    class Meta:
        verbose_name = _('Documento Solicitado')
        verbose_name_plural = _('Documentos Solicitados')

    def __str__(self):
        return f'{self.notice.title} - {self.name}'

    def is_required(self):
        return not self.optional
