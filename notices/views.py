from django.shortcuts import render, redirect, get_object_or_404
from .models import Notice, NoticeStep
from base.models import Step
from subscriptions.models import Resource, StepResult, Subscription
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages
from django.views.generic import TemplateView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth.mixins import UserPassesTestMixin
from django.core.exceptions import PermissionDenied
from django.views import View
from django.db.models import Avg
from .forms import *


class NoticeList(LoginRequiredMixin, ListView):
    model = Notice
    page_title = "Editais Relacionados"

    def get_queryset(self):
        return Notice.objects.filter(program__in=self.request.user.programs.all()).order_by('-created_at')

    def get_context_data(self, object_list=None, **kwargs):
        return super().get_context_data(object_list=object_list, page_title=self.page_title, **kwargs)

class NoticeListAll(LoginRequiredMixin, ListView):
    model = Notice
    page_title = "Todos os Editais"

    def get_queryset(self):
        return Notice.objects.all().order_by('-created_at')

    def get_context_data(self, object_list=None, **kwargs):
        return super().get_context_data(object_list=object_list, page_title=self.page_title, **kwargs)

class NoticeListFinished(LoginRequiredMixin, ListView):
    model = Notice
    page_title = "Editais Concluídos"

    def get_queryset(self):
        return Notice.objects.filter(status=Notice.Status.FINISHED)

    def get_context_data(self, object_list=None, **kwargs):
        return super().get_context_data(object_list=object_list, page_title=self.page_title, **kwargs)

class NoticeCreate(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    model = Notice
    form_class = NoticeForm
    template_name = 'notices/notice_create.html'
    success_url = reverse_lazy('notice_list')
    success_message = "Edital criado com sucesso!"

    def get(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        initial_step = Step.objects.get(name="Avaliação de Inscrição")

        vacancy_form = NoticeFormSet()
        noticestep_form = NoticeNoticeStepFormSet(initial=[{'step': initial_step, 'passing_score': 10.0 }])
        noticeattachment_form = NoticeNoticeAttachmentFormSet()
        requireddocument_form = NoticeRequiredDocumentFormSet()
        return self.render_to_response(
            self.get_context_data(  form = form,
                                    vacancy_form= vacancy_form,
                                    noticestep_form = noticestep_form,
                                    noticeattachment_form = noticeattachment_form,
                                    requireddocument_form = requireddocument_form))

    def post(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        
        vacancy_form = NoticeFormSet(self.request.POST)
        noticestep_form = NoticeNoticeStepFormSet(self.request.POST)
        noticeattachment_form = NoticeNoticeAttachmentFormSet(self.request.POST, self.request.FILES)
        requireddocument_form = NoticeRequiredDocumentFormSet(self.request.POST, self.request.FILES)

        if(form.is_valid() and vacancy_form.is_valid() and noticestep_form.is_valid() and noticeattachment_form.is_valid() and requireddocument_form.is_valid()):
            return self.form_valid(form, vacancy_form, noticestep_form, noticeattachment_form, requireddocument_form)
        else:
            return self.form_invalid(form, vacancy_form, noticestep_form, noticeattachment_form, requireddocument_form)

    def form_valid(self, form, vacancy_form, noticestep_form, noticeattachment_form, requireddocument_form):
        self.object = form.save()
        vacancy_form.instance = self.object
        vacancy_form.save()
        noticestep_form.instance = self.object
        noticestep_form.save()
        noticeattachment_form.instance = self.object
        noticeattachment_form.save()
        requireddocument_form.instance = self.object
        requireddocument_form.save()

        messages.success(self.request, self.success_message)
        return redirect(self.get_success_url())

    def form_invalid(self, form, vacancy_form, noticestep_form, noticeattachment_form, requireddocument_form):
        return self.render_to_response(
            self.get_context_data(  form = form,
                                    vacancy_form = vacancy_form,
                                    noticestep_form = noticestep_form,
                                    noticeattachment_form = noticeattachment_form,
                                    requireddocument_form = requireddocument_form))

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['programs'] = self.request.user.programs.all()
        return kwargs

    def test_func(self):
        if not self.request.user.programs.all().exists():
            raise PermissionDenied("Você não possui autorização para isto.")

        return True

class NoticeUpdate(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Notice
    form_class = NoticeForm
    template_name = 'notices/notice_update.html'
    success_url = reverse_lazy('notice_list')
    success_message = "Edital alterado com sucesso!"

    def get(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        vacancy_form = NoticeFormSet(instance=self.object)
        noticestep_form = NoticeNoticeStepFormSet(instance=self.object)
        noticeattachment_form = NoticeNoticeAttachmentFormSet(instance=self.object)
        requireddocument_form = NoticeRequiredDocumentFormSet(instance=self.object)
        return self.render_to_response(
            self.get_context_data(  form = form,
                                    vacancy_form = vacancy_form,
                                    noticestep_form = noticestep_form,
                                    noticeattachment_form = noticeattachment_form,
                                    requireddocument_form = requireddocument_form))

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        vacancy_form = NoticeFormSet(self.request.POST, instance=self.object)
        noticestep_form = NoticeNoticeStepFormSet(self.request.POST, instance=self.object)
        noticeattachment_form = NoticeNoticeAttachmentFormSet(self.request.POST, self.request.FILES, instance=self.object)
        requireddocument_form = NoticeRequiredDocumentFormSet(self.request.POST, self.request.FILES, instance=self.object)
        if(form.is_valid() and vacancy_form.is_valid() and noticestep_form.is_valid() and noticeattachment_form.is_valid() and requireddocument_form.is_valid()):
            return self.form_valid(form, vacancy_form, noticestep_form, noticeattachment_form, requireddocument_form)
        else:
            return self.form_invalid(form, vacancy_form, noticestep_form, noticeattachment_form, requireddocument_form)

    def form_valid(self, form, vacancy_form, noticestep_form, noticeattachment_form, requireddocument_form):
        self.object = form.save()
        vacancy_form.instance = self.object
        vacancy_form.save()
        noticestep_form.instance = self.object
        noticestep_form.save()
        noticeattachment_form.instance = self.object
        noticeattachment_form.save()
        requireddocument_form.instance = self.object
        requireddocument_form.save()
        
        messages.success(self.request, self.success_message)
        return redirect(self.get_success_url())

    def form_invalid(self, form, vacancy_form, noticestep_form, noticeattachment_form, requireddocument_form):
        return self.render_to_response(
            self.get_context_data(  form = form,
                                    vacancy_form = vacancy_form,
                                    noticestep_form = noticestep_form,
                                    noticeattachment_form = noticeattachment_form,
                                    requireddocument_form = requireddocument_form))

    def test_func(self):
        self.object = self.get_object()

        if not self.request.user.programs.filter(id=self.object.program.pk).exists():
            raise PermissionDenied("Você não possui autorização para isto.")

        return True

class NoticeView(LoginRequiredMixin, TemplateView):
    template_name = "notices/notice_view.html"

    def get(self, request, pk, *args, **kwargs):
        notice = get_object_or_404(Notice, pk=pk)
        subscriptions = Subscription.objects.filter(notice=notice)
        resources = Resource.objects.filter(step_result__notice_step__in=notice.notice_steps.all(), status=Resource.Status.PENDING)
        context = self.get_context_data(notice=notice, subscriptions=subscriptions, resources=resources)
        return self.render_to_response(context)

class NoticeCancel(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    success_url = reverse_lazy('notice_list')
    http_method_names = ['get']
    success_message = "Edital cancelado com sucesso!"
    error_message = "Este edital não está pendente ou já foi cancelado!"

    def get(self, request, pk, **kwargs):
        if self.notice != Notice.Status.CANCELED:
            self.notice.status = Notice.Status.CANCELED
            self.notice.save()
            messages.success(request, self.success_message)
        else:
            messages.error(request, self.error_message)

        return redirect(self.success_url)

    def test_func(self):
        self.notice = get_object_or_404(Notice, pk=self.kwargs['pk'])

        if not self.request.user.programs.filter(id=self.notice.program.pk).exists():
            raise PermissionDenied("Você não possui autorização para isto.")

        return True

class StepsResult(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    template_name = "notices/step_result_view.html"

    def get(self, request, pk, *args, **kwargs):
        steps = self.notice.notice_steps.all() 
        results = StepResult.objects.filter(notice_step__in=steps)
        context = self.get_context_data(notice=self.notice, results=results, steps=steps)
        return self.render_to_response(context)

    def test_func(self):
        self.notice = get_object_or_404(Notice, pk=self.kwargs['pk'])

        if not self.request.user.programs.filter(id=self.notice.program.pk).exists():
            raise PermissionDenied("Você não possui autorização para isto.")

        return True

class NoticeListOpen(LoginRequiredMixin, ListView):
    template_name = 'notices/notice_list_open.html'

    def get_queryset(self):
        courses = self.request.user.courses.all()
        posts = PostGraduationProgram.objects.filter(courses__in=[ user_course.course for user_course in courses])
        return Notice.objects.filter(program__in=posts, status=Notice.Status.PENDING)

class NoticeSearch(LoginRequiredMixin, ListView):
    template_name = 'notices/notice_list.html'

    def get_queryset(self):
        return Notice.objects.filter(title__contains=self.request.GET["key"])

class NoticeQuery(LoginRequiredMixin, TemplateView):
    template_name = 'notices/notice_query.html'

    def get(self, request, pk, *args, **kwargs):
        notice = get_object_or_404(Notice, pk=pk)
        return self.render_to_response(self.get_context_data(notice = notice))

class NoticeResult(LoginRequiredMixin, TemplateView):
    template_name = 'notices/notice_result.html'

    def get(self, request, pk, *args, **kwargs):
        notice = get_object_or_404(Notice, pk=pk)
        step = notice.notice_steps.last()
        subscriptions = [sr.subscription for sr in StepResult.objects.filter(notice_step=step, status=StepResult.Status.APPROVED)]
        context = self.get_context_data(notice=notice, subscriptions=subscriptions)
        return self.render_to_response(context)

class NoticeSubscriptions(LoginRequiredMixin, TemplateView):
    template_name = 'notices/notice_subscriptions.html'

    def get(self, request, pk, *args, **kwargs):
        notice = get_object_or_404(Notice, pk=pk)
        subscriptions = Subscription.objects.filter(notice=notice)
        context = self.get_context_data(notice=notice, subscriptions=subscriptions)
        return self.render_to_response(context)

class NoticeResultReport(LoginRequiredMixin, ListView):
    template_name = 'reports/notice_search_results.html'

    def get_queryset(self):
        key = self.request.GET.get("key")
        if key:
            return Notice.objects.filter(title__contains=key)
        return Notice.objects.all().order_by('created_at')[0:10]

class NoticeSubscriptionsReport(LoginRequiredMixin, ListView):
    template_name = 'reports/notice_search_subscriptions.html'

    def get_queryset(self):
        key = self.request.GET.get("key")
        if key:
            return Notice.objects.filter(title__contains=key)
        return Notice.objects.all().order_by('created_at')[0:10]

