from django.core.management.base import BaseCommand
from notices.models import Notice
from django.utils import timezone


class Command(BaseCommand):
    help = 'Update Notice status'

    def handle(self, *args, **kwargs):
        notices = Notice.objects.filter(status__in=[Notice.Status.PENDING, Notice.Status.IN_PROGRESS])

        now = timezone.now().date()
        for notice in notices:
            changed = False
            if notice.status == Notice.Status.PENDING and now >= notice.start_date and now < notice.end_date:
                notice.status = Notice.Status.IN_PROGRESS
                changed = True
            elif notice.status == Notice.Status.IN_PROGRESS and now >= notice.closing_date:
                notice.status = Notice.Status.FINISHED
                changed = True

            if changed:
                notice.save()
                self.stdout.write(self.style.SUCCESS(f"Notice {notice.id} status changed to {notice.get_status_display()}."))
