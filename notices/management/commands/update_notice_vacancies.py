from django.core.management.base import BaseCommand
from notices.models import Notice
from subscriptions.models import Subscription
from base.models import Modality
from django.utils import timezone
from django.db.models import Sum

class Command(BaseCommand):
    help = 'Update Notice vacancies'

    def handle(self, *args, **kwargs):
        notices = Notice.objects.filter(status=Notice.Status.IN_PROGRESS)

        for notice in notices:
            sum = notice.vacancies.filter(modality__type=Modality.Type.PROVED).aggregate(Sum('quantity'))['quantity__sum']
            if sum in [None, 0]:
                continue

            # Check if there is at least one subscription to a proved modality
            if not notice.subscriptions.filter(status=Subscription.Status.PENDING).exists() and \
                not notice.subscriptions.filter(status=Subscription.Status.ACCEPT, vacancy__modality__type=Modality.Type.PROVED).exists():

                vacancies = notice.vacancies.filter(modality__type=Modality.Type.PROVED)
                not_proved = notice.vacancies.get(modality__type=Modality.Type.NOT_PROVED)

                for vacancy in vacancies:
                    not_proved.quantity += vacancy.quantity
                    vacancy.quantity = 0
                    vacancy.save()

                not_proved.save()
                self.stdout.write(self.style.SUCCESS(f"Notice {notice.id} proved vacancies moved to not proved vacancy."))
