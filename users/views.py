from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic.list import ListView
from django.views.generic import CreateView, UpdateView
from django.views.generic.edit import FormView
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.views import LoginView as DjangoLoginView, LogoutView as DjangoLogoutView, PasswordChangeView
from django.contrib.auth import get_user_model
from django.urls import reverse_lazy
from .forms import *
from .models import Notification, User
from django.contrib.auth.mixins import UserPassesTestMixin
from django.core.exceptions import PermissionDenied

User = get_user_model()

class LoginView(DjangoLoginView):
    template_name = 'login.html'
    form_class = AuthForm
    redirect_authenticated_user = True

class LogoutView(LoginRequiredMixin, DjangoLogoutView):
    template_name = ''

class NotificationList(LoginRequiredMixin, ListView):
    model = Notification

    def get_queryset(self):
        return self.request.user.notifications.all().order_by('-status', '-created_at')

class SignUpView(SuccessMessageMixin, CreateView):
    model = User
    form_class = UserCreateForm
    template_name = 'signup.html'
    success_url = reverse_lazy('login')
    success_message = "Conta registrada com sucesso!"

    def get(self, request, *args, **kwargs):
        self.object = None

        form_class = self.get_form_class()
        form = self.get_form(form_class)

        usercourse_formset = UserUserCourseFormSet()

        context_data = self.get_context_data(usercourse_formset=usercourse_formset)
        return self.render_to_response(context_data)  

    def post(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()

        form = self.get_form(form_class)

        usercourse_formset = UserUserCourseFormSet(self.request.POST)

        if(form.is_valid() and usercourse_formset.is_valid()):
            return self.form_valid(form, usercourse_formset)
        else:
            return self.form_invalid(form, usercourse_formset)

    def form_valid(self, form, usercourse_formset):
        super().form_valid(form)
        self.object = form.save()
        usercourse_formset.instance = self.object
        usercourse_formset.save()
        return redirect(self.get_success_url())

    def form_invalid(self, form, usercourse_formset):
        return self.render_to_response(
            self.get_context_data(  form = form,
                                    usercourse_formset = usercourse_formset))

class UserUpdate(LoginRequiredMixin, UpdateView):
    model = User
    form_class = UserUpdateForm
    template_name = 'users/user_update.html'
    success_url = reverse_lazy('dashboard')
    success_message = "Conta atualizada com sucesso!"
    
    def get(self, request, *args, **kwargs):
        self.object = self.request.user

        form_class = self.get_form_class()
        form = self.get_form(form_class)

        usercourse_form = UserUserCourseFormSet(instance=self.object)
        context_data = self.get_context_data(form_password=UserPasswordForm(user=self.request.user), usercourse_form=usercourse_form)
        return self.render_to_response(context_data)  

    def post(self, request, *args, **kwargs):
        self.object = self.request.user
        form_class = self.get_form_class()

        form = self.get_form(form_class)

        usercourse_form = UserUserCourseFormSet(self.request.POST, instance=self.object)

        if(form.is_valid() and usercourse_form.is_valid()):
            return self.form_valid(form, usercourse_form)
        else:
            return self.form_invalid(form, usercourse_form)

    def form_valid(self, form, usercourse_form):
        self.object = form.save()
        usercourse_form.instance = self.object
        usercourse_form.save()
        messages.success(self.request, self.success_message)
        return redirect(self.get_success_url())

    def form_invalid(self, form, usercourse_form):
        return self.render_to_response(
            self.get_context_data(  form = form,
                                    usercourse_form = usercourse_form))

class UserPasswordUpdate(LoginRequiredMixin, SuccessMessageMixin, PasswordChangeView):
    form_class = UserPasswordForm
    success_url = reverse_lazy('profile_edit')
    success_message = "Senha atualizada com sucesso!"
    http_method_names = ['post']

    def form_valid(self, form):
        super().form_valid(form)
        return redirect(self.success_url)

    def form_invalid(self, form):
        for key in form.errors:
            messages.error(self.request, form.errors[key][0])
        return redirect(self.success_url)

class NotificationRead(LoginRequiredMixin, UserPassesTestMixin, View):
    success_url = reverse_lazy('notification_list')
    http_method_names = ['get']

    def get(self, request, pk, **kwargs):
        self.notification.status = Notification.Status.READ
        self.notification.save()
        return redirect(self.success_url)

    def test_func(self):
        self.notification = get_object_or_404(Notification, pk=self.kwargs['pk'])

        if not self.notification.user == self.request.user:
            raise PermissionDenied("Você não possui autorização para isto.")

        return True