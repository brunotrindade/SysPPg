from django.urls import path
from .views import NotificationList, UserUpdate, UserPasswordUpdate, NotificationRead
from django.views.generic import TemplateView


urlpatterns = [
    path('notifications/', NotificationList.as_view(), name='notification_list'),
    path('notifications/<uuid:pk>/read', NotificationRead.as_view(), name='notification_read'),
    path('update/', UserUpdate.as_view(), name='profile_edit'),
    path('password/', UserPasswordUpdate.as_view(), name='user_password_update'),
] 