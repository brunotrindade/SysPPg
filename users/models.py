from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.utils import timezone
from datetime import datetime
from .managers import UserManager
from base.models import BaseModel, UndergraduateCourse
from django.utils.translation import gettext as _
from base.models import PostGraduationProgram

class User(AbstractBaseUser, PermissionsMixin, BaseModel):
    class MaritalStatus(models.TextChoices):
        NOT_MARRIED = 'S', _('Solteiro(a)')
        MARRIED = 'C', _('Casado(a)')
        DIVORCED = 'D', _('Divorciado(a)')
        WIDOWER = 'V', _('Viúvo(a)')

    class Sex(models.TextChoices):
        MALE = 'M', _('Masculino')
        FEMALE = 'F', _('Feminino')
        OTHER = 'O', _('Outro')

    cpf = models.CharField(
        _('CPF'),
        unique=True,
        error_messages={'unique': _("Já existe um usuário com este CPF")},
        max_length=15
    )
    email = models.EmailField(
        _('Email'),
        unique=True,
        error_messages={'unique': _("Já existe um usuário com este email")},
    )
    full_name = models.CharField(_('Nome Completo'), max_length=255)
    address = models.CharField(_('Endereço Residencial'), max_length=90, null=True)
    phone_number = models.CharField(_('Telefone'), max_length=20, null=True)
    marital_status = models.CharField(_('Estado Civil'), max_length=1, choices=MaritalStatus.choices, 
        default=MaritalStatus.NOT_MARRIED, null=True)
    sex = models.CharField(_('Sexo'), max_length=1, choices=Sex.choices, default=Sex.MALE, null=True)
    date_of_birth = models.DateField(_('Data de nascimento'), null=True)
    is_staff = models.BooleanField(_('Membro da Equipe'), default=False)
    is_active = models.BooleanField(
        _('Ativo'), default=True, help_text=_('Desative para tirar o acesso do usuário')
    )
    programs = models.ManyToManyField(PostGraduationProgram, blank=True)

    objects = UserManager()

    USERNAME_FIELD = 'cpf'
    REQUIRED_FIELDS = ['full_name', 'email']

    class Meta:
        verbose_name = _('Usuário')
        verbose_name_plural = _('Usuários')

    def __str__(self):
        return f'{self.email} {self.full_name[:30]}'

    def get_short_name(self):
        parts = self.full_name.split(" ")
        return f"{parts[0]} {parts[-1]}"

    def is_coordinator(self):
        return self.programs.all().count() > 0

    def get_not_read_notifications(self):
        return self.notifications.filter(status=Notification.Status.NOT_READ)

    def get_formatted_cpf(self):
        return f"{self.cpf[0:3]}.{self.cpf[3:6]}.{self.cpf[6:9]}-{self.cpf[9:11]}"

class UserCourse(BaseModel):
    completion_date = models.DateField(_('Data de conclusão'))
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="courses")
    course = models.ForeignKey(UndergraduateCourse, on_delete=models.PROTECT)

    class Meta:
        verbose_name = _('Graduação de Usuário')
        verbose_name_plural = _('Graduações de Usuário')

    def __str__(self):
        return f'{self.completion_date}'

class Notification(BaseModel):
    class Status(models.TextChoices):
        NOT_READ = 'N', _('Não lida')
        READ = 'L', _('Lida')

    class Type(models.TextChoices):
        INFO = 'I', _('Informativo')
        SUCCESS = 'S', _('Sucesso')
        WARNING = 'A', _('Alerta')
        DANGER = 'D', _('Perigo')

    title = models.CharField(_('Título'), max_length=25)
    content = models.CharField(_('Conteúdo'), max_length=80)
    icon = models.CharField(_('Ícone'), max_length=20, default="info")
    status = models.CharField(_('Status'), max_length=1, choices=Status.choices, default=Status.READ)
    type = models.CharField(_('Tipo'), max_length=1, choices=Type.choices, default=Type.INFO)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="notifications")

    class Meta:
        verbose_name = _('Notificação')
        verbose_name_plural = _('Notificações')

    def __str__(self):
        return f'{self.title}: {self.content}'

    def get_color(self):
        if self.type == self.Type.INFO:
            return 'text-primary'
        elif self.type == self.Type.WARNING:
            return 'text-warning'
        elif self.type == self.Type.SUCCESS:
            return 'text-success'
        elif self.type == self.Type.DANGER:
            return 'text-danger'

    def get_background_color(self):
        if self.status == self.Status.READ:
            return 'bg-success'
        else:
            return 'bg-danger'

