from django import forms
from django.contrib.auth.forms import UsernameField
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import authenticate
from django.contrib import messages
from .models import UserCourse
from validate_docbr import CPF
from django.contrib.auth.forms import PasswordChangeForm
from sysppg import settings
from django.contrib.auth.password_validation import validate_password

User = get_user_model()

class UserForm(forms.ModelForm):
    error_messages = {
        'password_mismatch': _('The two password fields didnt match.'),
    }
    password1 = forms.CharField(
        label=_("Senha"), strip=False, widget=forms.PasswordInput(
            attrs={'autocomplete': 'new-password', 'class': 'form-control form-control-lg','placeholder': 'Senha'})
    )
    password2 = forms.CharField(
        label=_("Confirmar senha"), strip=False,
        widget=forms.PasswordInput(
            attrs={
                'autocomplete': 'new-password',
                'class': 'form-control form-control-lg',
                'placeholder': 'Confirmação de senha'
            }
        )
    )

    class Meta:
        model = User
        fields = ('full_name', 'email', 'password1', 'password2')
        widgets = {
            'email': forms.EmailInput(attrs={'class': 'form-control form-control-lg', 'placeholder': 'Email'}),
            'full_name': forms.TextInput(
                attrs={'class': 'form-control form-control-lg', 'placeholder': 'Nome completo'}
            )
        }

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'], code='password_mismatch',
            )
        return password2

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class AuthForm(forms.Form):
    cpf = forms.CharField(
        widget=forms.TextInput(
            attrs={'autofocus': True, 'class': 'form-control form-control-lg', 
                'placeholder': 'Digite seu CPF', 'data-mask': '000.000.000-00'}
        )
    )
    password = forms.CharField(
        label=_("Password"), strip=False,
        widget=forms.PasswordInput(
            attrs={
                'autocomplete': 'current-password',
                'class': 'form-control form-control-lg',
                'placeholder': 'Senha'
            }
        )
    )

    def __init__(self, request=None, *args, **kwargs):
        self.request = request
        self.user = None
        super().__init__(*args, **kwargs)

    def clean(self):
        username = self.cleaned_data.get('cpf')
        password = self.cleaned_data.get('password')
        if username is not None and password:
            self.user = authenticate(self.request, username=username, password=password)
            if self.user is None:
                raise forms.ValidationError(
                    _('Usuário ou senha inválido')
                )
        return self.cleaned_data

    def clean_cpf(self):
        return self.cleaned_data.get('cpf').replace(".", "").replace("-", "")

    def get_user(self):
        return self.user

class UserCreateForm(forms.ModelForm):
    password1 = forms.CharField(widget=forms.PasswordInput(
        attrs={'class': 'form-control form-control-lg'}
    ))
    password2 = forms.CharField(widget=forms.PasswordInput(
        attrs={'class': 'form-control form-control-lg'}
    ))
    date_of_birth = forms.DateField(input_formats=settings.DATE_INPUT_FORMATS, widget=forms.DateInput(
        attrs={'class': 'form-control form-control-lg', 'data-mask': '00/00/0000'}
    ))

    class Meta:
        model = User
        fields = ('full_name', 'address', 'phone_number', 'marital_status', 'sex', 'date_of_birth', 'cpf', 'email')
        widgets = {
            'email': forms.EmailInput(
                attrs={'class': 'form-control form-control-lg'}
            ),
            'cpf': forms.TextInput(
                attrs={'class': 'form-control form-control-lg',
                    'data-mask': '000.000.000-00'}
            ),
            'full_name': forms.TextInput(
                attrs = {'class': 'form-control form-control-lg'}
            ),
            'address': forms.TextInput(
                attrs = {'class': 'form-control form-control-lg'}
            ),
            'phone_number': forms.TextInput(
                attrs={'class': 'form-control form-control-lg',
                    'data-mask': '(00) 00000-0000'}
            ),
            'marital_status': forms.Select(
                attrs={'class': 'form-control form-control-lg'}
            ),
            'sex': forms.Select(
                attrs={'class': 'form-control form-control-lg'}
            )
        }

    def clean(self):
        cleaned_data = super().clean()
        password1 = cleaned_data.get('password1')
        password2 = cleaned_data.get('password2')

        if password1 != password2:
            raise forms.ValidationError(_('As senhas informadas não coincidem!'))

        return cleaned_data

    def clean_cpf(self):
        cpf = self.cleaned_data.get('cpf')
        if not CPF().validate(cpf):
            raise forms.ValidationError(_('O CPF é inválido!'))

        cpf = cpf.replace('.','')
        cpf = cpf.replace('-','')
        return cpf

    def clean_password1(self):
        password1 = self.cleaned_data.get('password1')
        try:
            validate_password(password1, self.instance)
        except forms.ValidationError as error:
            self.add_error('password1', error)
        return password1

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.save()
        return user

class UserCourseForm(forms.ModelForm):
    completion_date = forms.DateField(input_formats=settings.DATE_INPUT_FORMATS, widget=forms.DateInput(
        attrs={'class': 'form-control form-control-lg', 'data-mask': '00/00/0000'}
    ))

    class Meta:
        model = UserCourse
        fields = ('course', 'completion_date')
        widgets = {
            'course': forms.Select(
                attrs={'class': 'form-control form-control-lg'}
            )
        }
        
class UserUserCourseForm(forms.BaseInlineFormSet):
    def clean(self):
        pass

UserUserCourseFormSet = forms.inlineformset_factory(
    User, UserCourse, UserCourseForm, UserUserCourseForm, extra=1, can_delete=True
)

class UserUpdateForm(forms.ModelForm):
    date_of_birth = forms.DateField(input_formats=settings.DATE_INPUT_FORMATS, widget=forms.DateInput(
        attrs={'class': 'form-control form-control-lg text-center', 'data-mask': '00/00/0000'}
    ))

    class Meta:
        model = User
        fields = ('full_name', 'address', 'phone_number', 'marital_status', 'sex', 'date_of_birth', 'email')
        widgets = {
            'email': forms.EmailInput(
                attrs={'class': 'form-control form-control-lg text-center'}
            ),
            'full_name': forms.TextInput(
                attrs = {'class': 'form-control form-control-lg text-center'}
            ),
            'address': forms.TextInput(
                attrs = {'class': 'form-control form-control-lg'}
            ),
            'phone_number': forms.TextInput(
                attrs={'class': 'form-control form-control-lg text-center',
                    'data-mask': '(00) 00000-0000'}
            ),
            'marital_status': forms.Select(
                attrs={'class': 'form-control form-control-lg text-center'}
            ),
            'sex': forms.Select(
                attrs={'class': 'form-control form-control-lg text-center'}
            ),
        }

class UserPasswordForm(PasswordChangeForm):
    old_password = forms.CharField(
        strip=False,
        widget=forms.PasswordInput(attrs={'autocomplete': 'current-password', 'autofocus': True,
            'class': 'form-control form-control-lg text-center'}),
    )
    new_password1 = forms.CharField(
        widget=forms.PasswordInput(attrs={'autocomplete': 'new-password', 
            'class': 'form-control form-control-lg text-center'}),
        strip=False,
    )
    new_password2 = forms.CharField(
        strip=False,
        widget=forms.PasswordInput(attrs={'autocomplete': 'new-password', 
            'class': 'form-control form-control-lg text-center'}),
    )