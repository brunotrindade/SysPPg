from django.forms import BaseFormSet

class FormSetWithInstances(BaseFormSet):
    def __init__(self, *args, **kwargs):
        self.instances = kwargs.pop('instances')
        super(FormSetWithInstances, self).__init__(*args, **kwargs)

    def get_form_kwargs(self, index):
        form_kwargs = super(FormSetWithInstances, self).get_form_kwargs(index)
        if index < len(self.instances):
            form_kwargs['instance'] = self.instances[index]
        return form_kwargs