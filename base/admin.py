from django.contrib import admin
from .models import *

admin.site.register(UndergraduateCourse)
admin.site.register(PostGraduationProgram)
admin.site.register(Modality)
admin.site.register(Step)
admin.site.register(Teacher)

admin.site.site_header = "Sistema de Programas de Pós-graduação"
admin.site.index_title = "Gerenciamento"
admin.site.site_title = admin.site.site_header + " - Painel"
admin.site.site_url = None
