from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from notices.models import Notice
from subscriptions.models import Subscription
from django.utils import timezone

class DashboardView(LoginRequiredMixin, TemplateView):
    template_name = "dashboard.html"

    def get(self, request, *args, **kwargs):
        if request.user.is_staff:
            pending = Notice.objects.filter(status=Notice.Status.PENDING).count()
            in_progress = Notice.objects.filter(status=Notice.Status.IN_PROGRESS).count()
            finished = Notice.objects.filter(status=Notice.Status.FINISHED).count()
            canceled = Notice.objects.filter(status=Notice.Status.CANCELED).count()

            months = ["", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho",
                "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"]

            months_values = {}
            now = timezone.now().date()

            for month in range(1, 12+1):
                months_values[months[month]] = Subscription.objects.filter(created_at__month=month, created_at__year=2020).count()

            return render(request, self.template_name, self.get_context_data(
                pending=pending, in_progress=in_progress, finished=finished, canceled=canceled, months_values=months_values
            ))
        else:
            return redirect('notice_list_open')