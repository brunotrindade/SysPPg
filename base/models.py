import uuid
from django.db import models
from django.utils.translation import gettext as _

class BaseModel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

class UndergraduateCourse(BaseModel):
    name = models.CharField(_('Nome'), max_length=60, unique=True)

    class Meta:
        verbose_name = _('Curso de Graduação')
        verbose_name_plural = _('Cursos de Graduação')

    def __str__(self):
        return f'{self.name}'

class PostGraduationProgram(BaseModel):
    class Center(models.TextChoices):
        CCET = 'CCET', _('Centro de Ciências Exatas e Tecnológicas')
        CCJSA = 'CCJSA', _('Centro de Ciências Jurídicas e Sociais Aplicadas')
        CCSD = 'CCSD', _('Centro de Ciências da Saúde e do Desporto')
        CELA = 'CELA', _('Centro de Educação, Letras e Artes')
        CFCH = 'CFCH', _('Centro de Filosofia e Ciências Humanas')
        CCBN = 'CCBN', _('Centro de Ciências Biológicas e da Natureza')
        CEL = 'CEL', _('Centro de Educação')
        CMULTI = 'CMULTI', _('Centro Multidisciplinar')

    name = models.CharField(_('Nome'), max_length=100, unique=True)
    center = models.CharField(_('Centro'), max_length=6, choices=Center.choices, default=Center.CCET)
    courses = models.ManyToManyField(UndergraduateCourse, _('Curso'))

    class Meta:
        verbose_name = _('Programa de Pós-graduação')
        verbose_name_plural = _('Programas de Pós-graduação')

    def __str__(self):
        return f'[{self.center}] {self.name}'

class Modality(BaseModel):
    class Type(models.TextChoices):
        PROVED = 'C', _('Comprovada')
        NOT_PROVED = 'N', _('Não-comprovada')

    name = models.CharField(_('Nome'), max_length=70, unique=True)
    type = models.CharField(_('Tipo'), max_length=1, choices=Type.choices, default=Type.PROVED)

    class Meta:
        verbose_name = _('Modalidade de Vaga')
        verbose_name_plural = _('Modalidades de Vaga')

    def __str__(self):
        return f'{self.name}'

class Step(BaseModel):
    name = models.CharField(_('Nome'), max_length=60, unique=True)

    class Meta:
        verbose_name = _('Tipo de Etapa')
        verbose_name_plural = _('Tipos de Etapa')

    def __str__(self):
        return f'{self.name}'

    def is_subscription_step(self):
        return self.name == 'Avaliação de Inscrição'

class Teacher(BaseModel):
    name = models.CharField(_('Nome'), max_length=50)
    course = models.ManyToManyField(PostGraduationProgram, _('Mestrado'))

    class Meta:
        verbose_name = _('Professor')
        verbose_name_plural = _('Professores')

    def __str__(self):
        return f'{self.name}'