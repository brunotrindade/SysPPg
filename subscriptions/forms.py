from django import forms
from .models import Subscription, SentDocument, Resource, ResourceAnswer, StepResult, StepResultTeacher
from notices.models import RequiredDocument
from django.utils.translation import gettext_lazy as _
from base.models import Modality
from django.utils import timezone

class SubscriptionForm(forms.ModelForm):
    modality = forms.ModelChoiceField(required=True, queryset=None, widget=forms.Select(
        attrs={'class': 'form-control form-control-lg text-center'}
    ))

    check = forms.BooleanField(required = True)

    class Meta:
        model = Subscription
        fields = ('scholarship', 'stump_voucher', 'lattes', 'check')
        widgets = {
            'lattes': forms.TextInput(
                attrs={'class': 'form-control form-control-lg text-center'}
            )
        }

    def __init__(self, *args, **kwargs):
        self.notice = kwargs.pop('notice', None)
        super(SubscriptionForm, self).__init__(*args, **kwargs)

        if self.notice:
            self.fields['modality'].queryset = Modality.objects.filter(pk__in=
                [vacancy.modality.pk for vacancy in self.notice.vacancies.all()]
            )

        if self.instance.created_at:
            self.initial['modality'] = self.instance.vacancy.modality

    def clean(self):
        subscription_date = timezone.now().date()
        notice_date = self.notice.end_date
        
        if subscription_date > notice_date:
            raise forms.ValidationError(_("As inscrições para esse edital já foram encerradas"))

    def clean_modality(self):
        modality = self.cleaned_data['modality']
        if modality.type == Modality.Type.PROVED:
            if not self.cleaned_data['stump_voucher']:
                raise forms.ValidationError(f"A modalidade {modality.name} exige comprovação.")

        return modality

class SentDocumentForm(forms.ModelForm):
    class Meta:
        model = SentDocument
        fields = ('required_document', 'file', 'answer')
        widgets = {
            'answer': forms.TextInput(
                attrs={'class': 'form-control form-control-lg text-center'}
            )
        }

    def clean(self):
        try:
            required_document = self.initial.get('required_document') or self.instance.required_document
            if self.cleaned_data.get('file') is None and self.cleaned_data.get('answer') is None \
                and not required_document.optional:

                raise forms.ValidationError(_(f"{required_document.name} é obrigatório!"))
        except RequiredDocument.DoesNotExist:
            pass
        

class SubscriptionSentDocumentForm(forms.BaseInlineFormSet):
    def clean(self):
        for form in self.forms:
            form.clean()

class ResourceForm(forms.ModelForm):
    class Meta:
        model = Resource
        fields = ('text', 'file')
        widgets = {
            'text': forms.Textarea(
                attrs={'class': 'form-control form-control-lg', 'rows': '3'}
            )
        }

class ResourceAnswerForm(forms.ModelForm):
    class Meta:
        model = ResourceAnswer
        fields = ('status', 'new_score', 'text', 'file')
        widgets = {
            'status': forms.Select(
                attrs={'class': 'form-control form-control-lg text-center'}
            ),
            'new_score': forms.TextInput(
                attrs={'class': 'form-control form-control-lg text-center'}
            ),
            'text': forms.Textarea(
                attrs={'class': 'form-control form-control-lg', 'rows': '3'}
            ),
        }

class StepResultForm(forms.ModelForm):
    class Meta:
        model = StepResult
        fields = ('score', 'obs')
        widgets = {
            'score': forms.TextInput(
                attrs={'class': 'form-control form-control-lg text-center'}
            ),
            'obs': forms.Textarea(
                attrs={'class': 'form-control form-control-lg', 'rows': '5'}
            ),
        }

class StepResultTeacherForm(forms.ModelForm):
    class Meta:
        model = StepResultTeacher
        fields = ('score', 'teacher')
        widgets = {
            'score': forms.TextInput(
                attrs={'class': 'form-control form-control-lg text-center'}
            ),
            'teacher': forms.Select(
                attrs={'class': 'form-control form-control-lg'}
            )
        }
class StepResultStepResultTeacherForm(forms.BaseInlineFormSet):

    def clean(self):
        if any(self.errors):
            return

StepResultStepResultTeacherFormSet = forms.inlineformset_factory(
    StepResult, StepResultTeacher, StepResultTeacherForm, StepResultStepResultTeacherForm, extra=1, can_delete=True, min_num=2, validate_min=True
)