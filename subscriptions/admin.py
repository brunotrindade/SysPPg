from django.contrib import admin
from .models import *

admin.site.register(Subscription)
admin.site.register(SentDocument)
admin.site.register(StepResult)
admin.site.register(Resource)
admin.site.register(ResourceAnswer)
admin.site.register(StepResultTeacher)
