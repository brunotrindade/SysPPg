from django.db import models
from django.utils.translation import gettext as _
from notices.models import Notice, RequiredDocument, NoticeStep, Vacancy
from users.models import User
from base.models import BaseModel, Teacher
from django.utils import timezone
from sysppg.utils import validate_file_size, validate_file_extension

class Subscription(BaseModel):
    class Status(models.TextChoices):
        PENDING = 'P', _('Pendente')
        ACCEPT = 'D', _('Deferida')
        DENIED = 'I', _('Indeferida')
        CANCELED = 'C', _('Cancelada')

    date = models.DateTimeField(_('Data de inscrição'), default=timezone.now)
    stump_voucher = models.FileField(_('Comprovante de cota'), upload_to="uploads/", null=True, blank=True, validators=[validate_file_size, validate_file_extension])
    lattes = models.CharField(_("Currículo Lattes"), max_length=50)
    status = models.CharField(_("Status"), max_length=1, choices=Status.choices, default=Status.PENDING)
    scholarship = models.BooleanField(_("Bolsa Capes"))
    full_name = models.CharField(_("Nome completo"), max_length=255)
    email = models.EmailField(_("Email"), max_length=50)
    address = models.CharField(_('Endereço Residencial'), max_length=90)
    vacancy = models.ForeignKey(Vacancy, on_delete=models.PROTECT)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='subscriptions')
    notice = models.ForeignKey(Notice, on_delete=models.CASCADE, related_name='subscriptions')

    class Meta:
        verbose_name = _('Inscrição')
        verbose_name_plural = _('Inscrições')

    def __str__(self):
        return f'[{self.pk}] {self.full_name} - {self.notice.title}'

    def get_status_color(self):
        if self.status == Subscription.Status.PENDING:
            return 'bg-warning'
        elif self.status == Subscription.Status.DENIED:
            return 'bg-danger'
        elif self.status == Subscription.Status.CANCELED:
            return 'bg-secondary'
        else:
            return 'bg-success'

    def is_canceled(self):
        return self.status == Subscription.Status.CANCELED

    def get_avg(self):
        avg = 0

        for result in self.step_results.all():
            avg += result.score        

        return avg/self.step_results.count()
        
    def is_reproved(self):
        return self.status not in [Subscription.Status.PENDING, Subscription.Status.ACCEPT] or \
            self.step_results.filter(status=StepResult.Status.REPROVED).exists()

    def is_finished(self):
        return self.step_results.all().count() == self.notice.notice_steps.all().count()

class SentDocument(BaseModel):
    answer = models.CharField(_("Resposta"), max_length=150, blank=True)
    file = models.FileField(_('Arquivo'), upload_to='uploads/', null=True, blank=True, validators=[validate_file_size, validate_file_extension])
    required_document = models.ForeignKey(RequiredDocument, on_delete=models.CASCADE)
    subscription = models.ForeignKey(Subscription, on_delete=models.CASCADE, related_name='sent_documents')

    class Meta:
        verbose_name = _('Documento Enviado')
        verbose_name_plural = _('Documentos Enviados')

    def __str__(self):
        return f'{self.subscription.id}'

class StepResult(BaseModel):
    class Status(models.TextChoices):
        APPROVED = 'A', _('Aprovado')
        REPROVED = 'R', _('Reprovado')

    score = models.DecimalField(_('Nota'), max_digits=4, decimal_places=2)
    obs = models.CharField(_('Observações'), max_length=255, null=True, blank=True)
    subscription = models.ForeignKey(Subscription, on_delete=models.CASCADE, related_name="step_results")
    notice_step = models.ForeignKey(NoticeStep, on_delete=models.CASCADE, related_name="step_results")
    status = models.CharField(_("Status"), max_length=1, choices=Status.choices, default=Status.APPROVED)

    class Meta:
        verbose_name = _('Resultados de Etapa')
        verbose_name_plural = _('Resultados de Etapa')
        unique_together = ['subscription', 'notice_step']

    def __str__(self):
        return f'{self.subscription.id} - {self.notice_step.step.name}'

    def is_approved(self):
        return self.score >= self.notice_step.passing_score

class Resource(BaseModel):
    class Status(models.TextChoices):
        PENDING = 'P', _('Pendente')
        ACCEPT = 'A', _('Aceito')
        DENIED = 'R', _('Rejeitado')

    text = models.CharField(_('Texto'), max_length=255)
    file = models.FileField(_('Arquivo'), upload_to='uploads/', validators=[validate_file_size, validate_file_extension])
    status = models.CharField(_('Status'), max_length=1, choices=Status.choices, default=Status.PENDING)
    date = models.DateTimeField(_('Data de postagem'), default=timezone.now)
    step_result = models.OneToOneField(StepResult, on_delete=models.CASCADE, related_name="resources")

    class Meta:
        verbose_name = _('Recurso de Etapa')
        verbose_name_plural = _('Recursos de Etapa')

    def __str__(self):
        return f'{self.step_result.notice_step.step.name}'

    def is_pending(self):
        return self.status == Resource.Status.PENDING

class ResourceAnswer(BaseModel):
    class Status(models.TextChoices):
        ACCEPT = 'A', _('Aceito')
        DENIED = 'R', _('Rejeitado')

    text = models.CharField(_('Texto'), max_length=255)
    file = models.FileField(_('Arquivo'), upload_to='uploads/', null=True, blank=True, validators=[validate_file_size, validate_file_extension])
    status = models.CharField(_('Status'), max_length=1, choices=Status.choices, default=Status.DENIED)
    new_score = models.DecimalField(_('Nova nota'), max_digits=4, decimal_places=2, default=0)
    date = models.DateTimeField(_('Data de postagem'), default=timezone.now)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    resource = models.OneToOneField(Resource, on_delete=models.CASCADE, related_name="answer")

    class Meta:
        verbose_name = _('Resposta a Recurso')
        verbose_name_plural = _('Respostas a Recurso')

    def __str__(self):
        return f'{self.id} - {self.text}'

class StepResultTeacher(BaseModel):
    score = models.DecimalField(_('Nota'), max_digits=4, decimal_places=2)
    teacher = models.ForeignKey(Teacher, on_delete=models.PROTECT)
    step_result = models.ForeignKey(StepResult, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Avaliador')
        verbose_name_plural = _('Avaliadores')

    def __str__(self):
        return f'{self.teacher.name} - {self.step_result.notice_step.step.name}'
