from django.shortcuts import render, redirect, get_object_or_404
from notices.models import Notice
from .models import Subscription, Resource, StepResult, ResourceAnswer
from base.models import UndergraduateCourse, PostGraduationProgram
from .forms import *
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils import timezone
from django.db.models import Max
from django.views import View
from django.contrib import messages
from django.contrib.auth.mixins import UserPassesTestMixin
from django.core.exceptions import PermissionDenied
from sysppg.utils import merge_pdfs
import io
from django.http import FileResponse

class SubscriptionList(LoginRequiredMixin, ListView):
    model = Subscription
    template_name = 'subscriptions/subscriptions.html'

    def get_queryset(self):
        return self.request.user.subscriptions.all().order_by('-created_at')

class SubscriptionCreate(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    model = Subscription
    form_class = SubscriptionForm
    template_name = 'subscriptions/subscription_form.html'
    success_url = reverse_lazy('subscription_list')
    success_message = "Inscrição realizada com sucesso!"

    def get(self, request, *args, **kwargs):
        self.object = None
        pk = kwargs.get('pk')
        self.notice = get_object_or_404(Notice, pk=pk)

        form_class = self.get_form_class()
        form = self.get_form(form_class)

        self.notice.number_views += 1
        self.notice.save()

        sentdocument_formset = self.get_formset(self.notice)

        context_data = self.get_context_data(notice=self.notice, sentdocument_formset=sentdocument_formset)
        return self.render_to_response(context_data)  

    def post(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        sentdocument_formset = self.get_formset(self.notice)

        if(form.is_valid() and sentdocument_formset.is_valid()):
            return self.form_valid(form, sentdocument_formset)
        else:
            return self.form_invalid(form, sentdocument_formset)

    def form_valid(self, form, sentdocument_formset):
        form.instance.notice = self.notice
        form.instance.vacancy = self.notice.vacancies.get(modality=form.cleaned_data['modality'])
        user = self.request.user
        self.object = form.save(commit=False)

        subscription = self.object

        subscription.full_name = user.full_name
        subscription.email = user.email
        subscription.address = user.address
        subscription.user = self.request.user
        subscription.save()

        sentdocument_formset.instance = self.object
        sent_documents = sentdocument_formset.save()

        messages.success(self.request, self.success_message)
        return redirect(self.get_success_url())

    def form_invalid(self, form, sentdocument_formset):
        return self.render_to_response(
            self.get_context_data(  form = form,
                                    sentdocument_formset = sentdocument_formset))

    def get_formset(self, notice):
        qs = notice.required_documents.all()
        SubscriptionSentDocumentFormSet = forms.inlineformset_factory(
            Subscription, SentDocument, SentDocumentForm, SubscriptionSentDocumentForm, extra=len(qs), can_delete=False
        )

        initial = []
        for rd in qs:
            initial.append({ 'required_document': rd })

        if self.request.POST:
            return SubscriptionSentDocumentFormSet(self.request.POST, self.request.FILES, initial=initial)
        else:
            return SubscriptionSentDocumentFormSet(initial=initial)

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super(SubscriptionCreate, self).get_form_kwargs(*args, **kwargs)
        kwargs['notice'] = self.notice
        return kwargs

    def test_func(self):
        pk = self.kwargs.get('pk')
        self.notice = get_object_or_404(Notice, pk=pk)

        if not self.notice.can_subscribe():
            raise PermissionDenied("Não é possível fazer a inscrição neste edital.")
        elif self.request.user.subscriptions.filter(notice=self.notice).exists():
            raise PermissionDenied("Você já está inscrito neste edital.")

        return True

class SubscriptionUpdate(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Subscription
    form_class = SubscriptionForm
    template_name = 'subscriptions/subscription_form.html'
    success_url = reverse_lazy('notice_list')
    success_message = "Inscrição atualizada com sucesso!"

    def get(self, request, pk, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        sentdocument_formset = self.get_formset(self.object.notice, instance=self.object)

        context_data = self.get_context_data(notice=self.object.notice, sentdocument_formset=sentdocument_formset)
        return self.render_to_response(context_data)  

    def post(self, request, pk, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        sentdocument_formset = self.get_formset(self.object.notice, instance=self.object)

        if(form.is_valid() and sentdocument_formset.is_valid()):
            return self.form_valid(form, sentdocument_formset)
        else:
            return self.form_invalid(form, sentdocument_formset)

    def form_valid(self, form, sentdocument_formset):
        modality = form.cleaned_data['modality']
        form.instance.vacancy = form.instance.notice.vacancies.get(modality=modality)
        if modality.type == Modality.Type.NOT_PROVED:
            form.instance.stump_voucher = None

        self.object = form.save()

        sentdocument_formset.instance = self.object
        sent_documents = sentdocument_formset.save()

        messages.success(self.request, self.success_message)
        return redirect(self.get_success_url())

    def form_invalid(self, form, sentdocument_formset):
        return self.render_to_response(
            self.get_context_data(  form = form,
                                    sentdocument_formset = sentdocument_formset))

    def get_formset(self, notice, instance, **kwargs):
        qs = notice.required_documents.all()

        initial = []
        for rd in qs:
            if rd not in [ sd.required_document for sd in instance.sent_documents.all() ]:
                initial.append({ 'required_document': rd })

        SubscriptionSentDocumentFormSet = forms.inlineformset_factory(
            Subscription, SentDocument, SentDocumentForm, SubscriptionSentDocumentForm, extra=len(qs), can_delete=False
        )

        if self.request.POST:
            return SubscriptionSentDocumentFormSet(self.request.POST, self.request.FILES, initial=initial, instance=instance, **kwargs)
        else:
            return SubscriptionSentDocumentFormSet(initial=initial, instance=instance, **kwargs)

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super(SubscriptionUpdate, self).get_form_kwargs(*args, **kwargs)
        kwargs['notice'] = self.object.notice
        return kwargs

    def test_func(self):
        self.object = self.get_object()

        if not self.object.notice.can_subscribe():
            raise PermissionDenied("Não é mais possível alterar esta inscrição.")
        elif self.object.user != self.request.user:
            raise PermissionDenied("Você não possui autorização para alterar esta inscrição.")

        return True

class SubscriptionView(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    template_name = "subscriptions/subscription_view.html"

    def get(self, request, pk, *args, **kwargs):
        resources = Resource.objects.filter(step_result__subscription=self.subscription)
        context = self.get_context_data(subscription=self.subscription, resources=resources)
        return self.render_to_response(context)

    def test_func(self):
        self.subscription = get_object_or_404(Subscription, pk=self.kwargs['pk'])
        is_coordinator = self.request.user.programs.filter(pk=self.subscription.notice.program.pk).exists()
        if self.subscription.user != self.request.user and not is_coordinator:
            raise PermissionDenied("Você não possui autorização para visualizar esta inscrição.")

        return True

class SubscriptionCancel(LoginRequiredMixin, UserPassesTestMixin, SuccessMessageMixin, View):
    success_message = "Inscrição cancelada com sucesso!"
    fail_message_pending = "A inscrição não está pendente e não pode ser cancelada."
    fail_message_canceled = "A inscrição já foi cancelada."

    def get(self, request, pk, *args, **kwargs):
        if self.subscription.status == Subscription.Status.PENDING:
            self.subscription.status = Subscription.Status.CANCELED
            self.subscription.save()
            messages.success(self.request, self.success_message)
        elif self.subscription.status == Subscription.Status.CANCELED:
            messages.error(self.request, self.fail_message_canceled)
        else:
            messages.error(self.request, self.fail_message_pending)

        return redirect('subscription_view', pk=pk)

    def test_func(self):
        self.subscription = get_object_or_404(Subscription, pk=self.kwargs['pk'])

        if not self.subscription.notice.can_subscribe():
            raise PermissionDenied("Não é mais possível cancelar esta inscrição.")
        elif self.subscription.user != self.request.user:
            raise PermissionDenied("Você não possui autorização para cancelar esta inscrição.")

        return True

class ResourceCreate(LoginRequiredMixin, UserPassesTestMixin, SuccessMessageMixin, CreateView):
    model = Resource
    form_class = ResourceForm
    template_name = 'resources/resource_create.html'
    success_url = reverse_lazy('subscription_list')
    success_message = "Recurso criado com sucesso!"

    def get(self, request, pk, *args, **kwargs):
        self.object = None
        notice = self.step_result.notice_step.notice
        return super().render_to_response(self.get_context_data(
            step_result=self.step_result,
            notice=notice
        ))

    def form_valid(self, form):
        form.instance.step_result = self.step_result
        return super().form_valid(form)

    def test_func(self):
        self.step_result = get_object_or_404(StepResult, pk=self.kwargs['pk'])

        if not self.step_result.notice_step.can_send_resource():
            raise PermissionDenied("Não é mais permitido submeter recurso nesta etapa.")
        elif self.step_result.subscription.user != self.request.user:
            raise PermissionDenied("Você não possui autorização para isto.")

        return True

class ResourceUpdate(LoginRequiredMixin, UserPassesTestMixin, SuccessMessageMixin, CreateView):
    model = Resource
    form_class = ResourceForm
    template_name = 'resources/resource_update.html'
    success_url = reverse_lazy('subscription_list')
    success_message = "Recurso atualizado com sucesso!"

    def get(self, request, pk, *args, **kwargs):
        self.object = get_object_or_404(Resource, pk=self.kwargs['pk'])
        resource = self.object
        step_result = resource.step_result
        notice = step_result.notice_step.notice
        return super().render_to_response(self.get_context_data(
            step_result=step_result,
            notice=notice
        ))

    def form_valid(self, form):
        return super().form_valid(form)

    def test_func(self):
        self.step_result = get_object_or_404(StepResult, pk=self.kwargs['pk'])

        if not self.step_result.notice_step.can_send_resource():
            raise PermissionDenied("Não é mais permitido atualizar este recurso.")
        elif self.step_result.subscription.user != self.request.user:
            raise PermissionDenied("Você não possui autorização para isto.")

        return True

class ResourceAnswerCreate(LoginRequiredMixin, UserPassesTestMixin, SuccessMessageMixin, CreateView):
    model = ResourceAnswer
    form_class = ResourceAnswerForm
    template_name = 'resources/resource_answer_create.html'
    success_url = reverse_lazy('notice_list')
    success_message = "Recurso avaliado com sucesso!"

    def get(self, request, pk, *args, **kwargs):
        self.object = None
        subscription = self.resource.step_result.subscription
        notice = self.resource.step_result.notice_step.notice
        self.initial['new_score'] = self.resource.step_result.score
        return super().render_to_response(self.get_context_data(
            resource=self.resource,
            subscription=subscription,
            notice=notice,
            date=timezone.now
        ))

    def form_valid(self, form):
        form.instance.resource = self.resource
        form.instance.user = self.request.user
        form.instance.resource.status = Resource.Status.ACCEPT if form.instance.status == ResourceAnswer.Status.ACCEPT else Resource.Status.DENIED
        form.instance.resource.save()

        step_result = form.instance.resource.step_result
        step_result.score = form.instance.new_score
        if form.instance.new_score >= step_result.notice_step.passing_score:
            step_result.status = StepResult.Status.APPROVED

            if self.resource.step_result.notice_step.step.is_subscription_step():
                self.resource.step_result.subscription.status = Subscription.Status.ACCEPT
                self.resource.step_result.subscription.save()
        
        step_result.save()
        return super().form_valid(form)

    def test_func(self):
        self.resource = get_object_or_404(Resource, pk=self.kwargs['pk'])

        if not self.request.user.programs.filter(id=self.resource.step_result.subscription.notice.program.pk).exists():
            raise PermissionDenied("Você não possui autorização para isto.")
        
        try:
            if self.resource.answer is not None:
                raise PermissionDenied("Este recurso já foi respondido.")
        except ResourceAnswer.DoesNotExist:
            pass

        return True

class StepResultCreate(LoginRequiredMixin, UserPassesTestMixin, SuccessMessageMixin, CreateView):
    model = StepResult
    form_class = StepResultForm
    template_name = 'subscriptions/step_result_create.html'
    success_url = reverse_lazy('notice_list')
    success_message = "Inscrição avaliada com sucesso!"

    def get(self, request, pk, *args, **kwargs):
        self.object = None
        notice = self.subscription.notice

        teacher_form = StepResultStepResultTeacherFormSet

        return super().render_to_response(self.get_context_data(
            subscription=self.subscription,
            notice=notice,
            notice_step=self.notice_step,
            date=timezone.now,
            teacher_form = teacher_form
        ))

    def post(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)

        teacher_form = StepResultStepResultTeacherFormSet(self.request.POST)

        if(form.is_valid() and teacher_form.is_valid()):
            return self.form_valid(form, teacher_form)
        else:
            return self.form_invalid(form, teacher_form)


    def form_valid(self, form, teacher_form):
        form.instance.subscription = self.subscription
        form.instance.notice_step = self.notice_step
        if form.instance.score < self.notice_step.passing_score:
            form.instance.status = StepResult.Status.REPROVED

        if form.instance.notice_step.step.is_subscription_step():
            if form.instance.status == StepResult.Status.REPROVED:
                self.subscription.status = Subscription.Status.DENIED
            else:
                self.subscription.status = Subscription.Status.ACCEPT

            self.subscription.save()

        teacher_form.instance = form.save()
        tfs = teacher_form.save(commit=True)

        #for tf in tfs:
           # tf.step_result = form.instance.step_result
        
        return super().form_valid(form)

    def form_invalid(self, form, teacher_form):
        return self.render_to_response(
            self.get_context_data(  form = form,
                                teacher_form = teacher_form))

    def get_next_step(self, subscription):
        try:
            next_order = self.subscription.step_results.latest('notice_step__order').notice_step.order + 1
        except StepResult.DoesNotExist:
            next_order = 1

        return self.subscription.notice.notice_steps.get(order=next_order)

    def test_func(self):
        self.subscription = get_object_or_404(Subscription, pk=self.kwargs['pk'])
        self.notice_step = self.get_next_step(self.subscription)

        if not self.request.user.programs.filter(id=self.subscription.notice.program.pk).exists():
            raise PermissionDenied("Você não possui autorização para isto.")
        elif self.subscription.step_results.filter(notice_step=self.notice_step).exists():
            raise PermissionDenied("Já existe um resultado para esta etapa.")

        return True

class SubscriptionDownload(LoginRequiredMixin, UserPassesTestMixin, View):
    def get(self, request, pk, *args, **kwargs):
        files = [ sd.file.path for sd in self.subscription.sent_documents.all() if bool(sd.file) ]
        buffer = io.BytesIO()
        merge_pdfs(files, buffer)
        buffer.seek(0)
        filename = f"documentos_{str(self.subscription.pk)[:8]}.pdf"
        return FileResponse(buffer, as_attachment=True, filename=filename)

    def test_func(self):
        self.subscription = get_object_or_404(Subscription, pk=self.kwargs['pk'])
        is_coordinator = self.request.user.programs.filter(pk=self.subscription.notice.program.pk).exists()
        if self.subscription.user != self.request.user and not is_coordinator:
            raise PermissionDenied("Você não possui autorização para visualizar esta inscrição.")

        return True