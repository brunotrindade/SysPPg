from django.urls import path, include
from django.conf.urls.static import static, settings
from django.views.generic import TemplateView
from .views import *

urlpatterns = [
    path('', SubscriptionList.as_view(), name="subscription_list"),
    path('<uuid:pk>/update', SubscriptionUpdate.as_view(), name="subscription_update"),
    path('<uuid:pk>/cancel', SubscriptionCancel.as_view(), name="subscription_cancel"),
    path('<uuid:pk>/result', StepResultCreate.as_view(), name="subscription_result"),
    path('<uuid:pk>/download', SubscriptionDownload.as_view(), name="subscription_download"),
    path('<uuid:pk>/', SubscriptionView.as_view(), name="subscription_view"),
]